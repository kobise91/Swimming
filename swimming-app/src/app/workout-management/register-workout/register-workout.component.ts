import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Workout } from '../../shared/models/workout.model';
import { WorkoutPart } from '../../shared/models/workoutPart.model';
import * as moment from 'moment';

@Component({
  selector: 'app-register-workout',
  templateUrl: './register-workout.component.html',
  styleUrls: ['./register-workout.component.css']
})
export class RegisterWorkoutComponent implements OnInit {
  // loginForm: FormGroup;

  @Input() workout:Workout = new Workout(null,null,null,moment(),[ new WorkoutPart("Warmup","test",[])],null);
  @Output() workoutChange = new EventEmitter();
  
  value = 'Clear me';


  constructor() { 
  }

  ngOnInit(): void {
  }

}
