import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Workout } from '../shared/models/workout.model';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkoutPart } from '../shared/models/workoutPart.model';
import * as workoutsReducers from '../store/reducers/workouts.reducers';
import { DeepCopy } from '../shared/Utilities';
import * as moment from 'moment';
import { Exercise } from '../shared/models/exercise.model';
@Component({
  selector: 'app-workout-management',
  templateUrl: './workout-management.component.html',
  styleUrls: ['./workout-management.component.css']
})
export class WorkoutManagementComponent implements OnInit {
  workout$: Observable<Workout> ;
  workoutAfetrChange:Workout= null;
  action:string;
  workoutDate:moment.Moment;
  constructor(
    private store: Store<{ workouts: workoutsReducers.State }>,  
    private route: ActivatedRoute,
    private router: Router
    ) {
        this.workoutDate = moment(router.getCurrentNavigation().extras?.state?.date);
        // this.workoutDate = this.workoutDate?? moment();
        console.log(this.workoutDate);
   }

  ngOnInit(): void {
    if( this.route.snapshot.routeConfig.path == "edit"){
        this.action = "edit";
        this.workout$= this.store.select(state => state.workouts.singleWorkout);
        this.workout$.subscribe(
          w=> this.workoutAfetrChange=DeepCopy(w)
        );
      }else{
        this.action = "add";
        this.workoutAfetrChange = new Workout(null,null,null,this.workoutDate,[ new WorkoutPart(null,null,[ new Exercise(null,null,null,[])])],null);
 
      }
  }

}
