import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Workout } from 'src/app/shared/models/workout.model';
import { WorkoutPart } from 'src/app/shared/models/workoutPart.model';
import {CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragStart} from '@angular/cdk/drag-drop';
import { Exercise } from 'src/app/shared/models/exercise.model';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-workout-plan',
  templateUrl: './workout-plan.component.html',
  styleUrls: ['./workout-plan.component.css']
})
export class WorkoutPlanComponent implements OnInit , OnDestroy {
  notifier = new Subject()

  @Input() workout:Workout = new Workout(null,null,null,moment(),[ ],null);
  @Output() workoutChange = new EventEmitter();
  
  direction:string="ltr";
  exercisesDeleteAraeHidden: [] = [];
  constructor(public translate: TranslateService) {
    this.translate.get('direction').subscribe((res: any) => {
      this.direction= res;
  }); 

  }
  ngOnDestroy(): void {        
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.translate.onLangChange.pipe(takeUntil(this.notifier)).subscribe( x => {
      console.log(x);
      this.translate.get('direction').subscribe((res: any) => {
        this.direction= res;
    }); 
    });
    for (let wpi = 0; wpi < this.workout.workoutParts.length; wpi++) {
      this.exercisesDeleteAraeHidden['workoutPart-exercises-'+wpi+'-drop-list'] = true;
    } 
    
  }

  updateDistances(){
    this.workout.workoutParts.every(workoutPart=> 
      workoutPart.totalDistance = workoutPart.exercises
      .reduce((sum, exercise) => sum + exercise.repetition*exercise.distance, 0));
    this.workout.totalDistance = this.workout.workoutParts
                        .reduce((sum, workoutPart) => sum + workoutPart.totalDistance, 0);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  delete(event: CdkDragDrop<string[]>) {
    // console.log(event);
    event.previousContainer.data.splice(event.previousIndex, 1);
    this.updateDistances();
  }

  draggingStarted(event:CdkDragStart){
    // console.log(event);
    this.exercisesDeleteAraeHidden[event.source.dropContainer.id] = false;
  }

  draggingEnded(event:CdkDragStart){
    // console.log(event);
    this.exercisesDeleteAraeHidden[event.source.dropContainer.id] = true;
  }
  
  addNewExercise(exercises)
  {
    let newEmptyExercise:Exercise=new Exercise(null,null,null,[]);
    exercises.push(newEmptyExercise);
  }
  AddNewWorkoutPar(){
    let newWorkoutPart:WorkoutPart=new WorkoutPart(null,null,[]);
    this.workout.workoutParts.push(newWorkoutPart);
    var wpi= this.workout.workoutParts.length-1;
    this.exercisesDeleteAraeHidden['workoutPart-exercises-'+wpi+'-drop-list'] = true;

  }
}
