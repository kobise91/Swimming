import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CalendarView, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarMonthViewDay } from 'angular-calendar';
import { Subject, Observable } from 'rxjs';
import { CustomEventTitleFormatter } from './custom-event-title-formatter.provider';
import { TranslateService } from '@ngx-translate/core';
import { registerLocaleData } from '@angular/common';
import localeHe from '@angular/common/locales/he';
import { Workout } from '../shared/models/workout.model';
import { Store } from '@ngrx/store';
import { LoadWorkouts, LoadSingleWorkout } from '../store/actions/workouts.actions';
import { ActivatedRoute, Router } from '@angular/router';
import * as workoutsReducers from '../store/reducers/workouts.reducers';
import * as moment from 'moment';
registerLocaleData(localeHe);

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'workout-calender',
  templateUrl: './workout-calender.component.html',
  styleUrls: ['./workout-calender.component.css'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
  ],
})
export class WorkoutCalenderComponent implements OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<span class="material-icons">create</span>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<span class="material-icons">delete</span>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = 
  [
    // {
    //   start: subDays(startOfDay(new Date()), 1),
    //   end: addDays(new Date(), 1),
    //   title: 'A 3 day event',
    //   color: colors.red,
    //   actions: this.actions,
    //   allDay: true,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true,
    //   },
    //   draggable: true,
    // },
    // {
    //   start: startOfDay(new Date()),
    //   title: 'An event with no end date',
    //   color: colors.yellow,
    //   actions: this.actions,
    // },
    // {
    //   start: subDays(endOfMonth(new Date()), 3),
    //   end: addDays(endOfMonth(new Date()), 3),
    //   title: 'A long event that spans 2 months',
    //   color: colors.blue,
    //   allDay: true,
    // },
    
    // {
    //   start: new Date(new Date().setHours(10)),
    //   end: new Date(new Date().setHours(12)),
    //   title: 'A draggable and resizable event2',
    //   color: colors.yellow,
    //   actions: this.actions,
    //   resizable: {
    //     beforeStart: false,
    //     afterEnd: false,
    //   },
    //   draggable: false,
    // },
  ];

  activeDayIsOpen: boolean = true;

  constructor(
    public translate: TranslateService,
    private store: Store<{ workouts: workoutsReducers.State }>,  
    private router: Router ) {
    
  }

  ngOnInit() {
    this.store.subscribe(s=> this.updateEvents(s.workouts.workouts));
    this.store.dispatch({ type: LoadWorkouts });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (this.isSameMonth(date, this.viewDate)) {
      if (
        (this.isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }
  isSameMonth(date1:Date, date2:Date){
      return date1.getMonth() == date2.getMonth() 
      && date1.getFullYear() == date2.getFullYear();
  }

  isSameDay(date1:Date, date2:Date){
    return date1.getDay() == date2.getDay() 
    && date1.getMonth() == date2.getMonth() 
    && date1.getFullYear() == date2.getFullYear();
  }
  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    if( action == 'Edited'){
      var newWorkout = new Workout(null,null,null,null,null,null);
      this.store.dispatch( { type:LoadSingleWorkout, workout:event.meta} );
      this.router.navigate(['workout','edit']);

    }
    // this.modal.open(this.modalContent, { size: 'lg' });
  }

  updateEvents(workouts:Workout[]){
    this.events = workouts.map(w=>
      {
        var startDate= new Date(moment(w.date).format());
        var endDate= new Date(moment(w.date).add(w.duration, 'minutes').format());
          return {
            start: startDate,
            end: endDate,
            title:w.trainerName +": "+ w.goalOfTraining,
            color: colors.yellow,
            actions: this.actions,
            resizable: {
              beforeStart: false,
              afterEnd: false,
            },
            draggable: false,
            meta: w
          }
      }
      );
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  doubleClick(day: CalendarMonthViewDay) {
    console.log('Day was double clicked', day);
    this.router.navigate(['workout','add'],{state:  {date: day.date  }});

  }
}