import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  direction:string="ltr";
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'he']);
    translate.setDefaultLang('he');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|he/) ? browserLang : 'he');
    this.translate.get('direction').subscribe((res: any) => {
      this.direction= res;
    });
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.direction= event.translations['direction'];
    });

  }

  ngOnInit(): void {
  }
}
