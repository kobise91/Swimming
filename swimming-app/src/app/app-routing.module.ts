import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WorkoutCalenderComponent } from './workout-calender/workout-calender.component';
import { WorkoutManagementComponent } from './workout-management/workout-management.component';

const routes: Routes = [
  { path: '', pathMatch:'full', redirectTo: '/home'},
  { path: 'home', component: HomepageComponent},
  { path: 'calendar', component: WorkoutCalenderComponent },  
  { path: 'workout', children:[
      { path: '', pathMatch:'full', redirectTo: '/home/calender'},
      { path: 'edit', component: WorkoutManagementComponent },
      { path: 'add', component: WorkoutManagementComponent }
    ]
  },
  { path: '**', component: PageNotFoundComponent },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
